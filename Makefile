SHELL := /bin/bash

# ==================================================
# COMMANDS

CC = gcc
RM = rm -f

# ==================================================
# DIRECTORIES

SRC = src
BIN = bin

# ==================================================
# TARGETS

SOURCES = $(wildcard $(SRC)/*.c)
OBJECTS = $(SOURCES:.c=.o)
TARGET 	= $(BIN)/myshell

# ==================================================
# COMPILATION

all: $(TARGET)

# -- myshell
$(TARGET): $(OBJECTS)
	$(CC) -o $(TARGET) $(OBJECTS)

# -- dependencies
%.o: $(SRC)/%.c
	$(CC) -c $< -o $@

clean:
	$(RM) $(SRC)/*.o $(SRC)/*~ *~

purge: clean
	$(RM) $(addprefix $(BIN)/, $(EXEC))
