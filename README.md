# MyShell
MyShell is a custome CLI, able of running the basic linux commands and applying redirection and pipelining to them.

## Getting Started
MyShell has an *interactive* mode and a *batch* mode. Once you have a copy on your local machine, run
```bash
make
make clean
```
to obtain the executable and then for *interactive* mode type:
```bash
bin/myshell
```
and for executing a *batchfile* in batch mode type:
```bash
bin/myshell batchfile
```

## Commands Format and Restrictions
You can execute any command providing the path to it's executable file (e.g. `./path/to/my/command`). For the known linux commands this path is already registered and you can *call them directly* (e.g. `ls`).

Lines are splitted into commands by the following characters and only: `;`, `&&` and `|`. There is no *fraud checking* so you have to be careful when typing.
* the character `;` is used to seperate the commands and sign command execution independently of the success or failure of the previous command
* the character `&&` is used to seperate the commands and sign command execution if and only if the previous command was successful
* the character `|` is used for pipeling: it uses the output of the previous command as an input for the next command (instead of printing in `stdout` and using `stdin` for input parsing)

You can redirect the input and output of commands using the redirection operators (`<<` and `>>`) provided that there is no input redirection (`<<`) following a output redirection (`>>`). The reason for this is that you *cannot* redirect the content of a file but only the output of a command. You also *cannot* use input redirection to edit the contents of a file (e.g. `file1 < file2` and `file1 > file2` are not supported).

### Valid Command Examples
```bash
ls -l
pwd
ps
touch hello
ls -l ; cat file1 ; grep foo file2
ls -l && cat file
echo "hello" > file
grep file < file1 < file2
ls -l > file1 && grep file2 < file1 | tee file3 file4 > /dev/null
ls -l | grep user | grep file
cd ~/Desktop
quit
```

### Invalid Command Examples
```bash
file1 > file2
file1 < file2
echo 'hello' > file1 < file2
customecommandwithoutapath
```

## Built With
* [VS Code](https://code.visualstudio.com/) - *The Code Editor*
* [gcc](https://gcc.gnu.org/) - *The C language compiler*

## Future Work
There is no guaranted support for this project, yet the following additions could improve the overall structure of the code:
* fraud checking
* use of redirection just for files
* use of the `<<` and `>>` operators
* support of built-in commands besides `quit` and `cd`

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments
This custom *CLI* is made for the *Operating Systems* course of the ECE Department of Aristotle University of Thessaloniki.