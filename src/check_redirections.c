#include "myshell.h"

char** check_redirections(char *command) {
    int input_red_counter=0, output_red_counter=0;
    char *ch, *cmd, **files = (char **) malloc(2 * sizeof(char*));
    int first_input_red_idx=-1, last_input_red_idx=-1, first_output_red_idx=-1, last_output_red_idx=-1;

    for (ch = command; *ch != NULL; ch++) {
        if (*ch == '<') {
            if (first_input_red_idx == -1)
                first_input_red_idx = ch-command;
            else
                last_input_red_idx = ch-command;
            input_red_counter++;
        } else if (*ch == '>') {
            if (first_output_red_idx == -1)
                first_output_red_idx = ch-command;
            else
                last_output_red_idx = ch-command;
            output_red_counter++;
        }
    }

    /* Handle case where there is a single input/output redirection*/
    if (last_input_red_idx == -1)
        last_input_red_idx = first_input_red_idx;
    if (last_output_red_idx == -1)
        last_output_red_idx = first_output_red_idx;

    /* Handle case where there is erroneous redirection */
    if (last_input_red_idx != -1 && first_output_red_idx != -1 && last_input_red_idx >= first_output_red_idx) {
        printf("%s: Can't support weird redirections\n", SHELL_NAME);
        exit(EXIT_FAILURE);
    }

    /* Keep only the executable part of the command */
    cmd = strdup(command);
    if (first_input_red_idx != -1)
        command[first_input_red_idx] = '\0';
    else if (first_output_red_idx != -1)
        command[first_output_red_idx] = '\0';
        
    /* Process files */
    ch = strtok_r(cmd, "<>", &cmd);
    ch = strtok_r(cmd, "< >", &cmd);
    for (int counter = 1; ch != NULL; counter++) {
        FILE *file;

        /* Check if this file is the final input or output file */
        if (counter == input_red_counter) {
            file = fopen(ch, "a");
            
            files[0] = (char *) malloc((strlen(ch) + 1) * sizeof(char));
            strcpy(files[0], ch);
        } else if (counter == input_red_counter + output_red_counter) {
            file = fopen(ch, "a");

            files[1] = (char *) malloc((strlen(ch) + 1) * sizeof(char));
            strcpy(files[1], ch);
        } else {
            file = fopen(ch, "w");
        }

        if (file == NULL) {
            printf(stderr, "%s: Can't create/open file %s\n", SHELL_NAME, ch);
            exit(EXIT_FAILURE);
        }

        fclose(file);

        /* Procede to next file */
        ch = strtok_r(cmd, "< >", &cmd);
    }

    /* If there are no input or output files, assign std */
    if (first_input_red_idx == -1) {
        files[0] = (char *) malloc(6 * sizeof(char));
        strcpy(files[0], "_stdin");
    }
    if (first_output_red_idx == -1) {
        files[1] = (char *) malloc(7 * sizeof(char));
        strcpy(files[1], "_stdout");
    }

    return files;
}