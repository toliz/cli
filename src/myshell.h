#ifndef MYSHELL_H
#define MYSHELL_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <fcntl.h>
#include <pwd.h>

#define TRUE 1
#define FALSE 0

#define SHELL_NAME "myshell"
#define PROMPT_MSG "panagiotopoulos 8880>"

#define ARG_SIZE 32
#define INPUT_SIZE 512

char* clear_string(char* str, char character);

char** split_line(char *line, const char *delimiters);

char** check_redirections(char *command);

void execute(char *command, int* in, int* out, char *filename, int command_no, int batchmode);

#endif /* MYSHELL_H */