#include "myshell.h"

void execute(char *command, int* in, int* out, char *filename, int command_no, int batchmode) {
    char **files = check_redirections(command);
    char **argv = split_line(command, " ");

    /* Input redirection */
    if (in[0] != STDIN_FILENO && !strcmp(files[0], "_stdin")) {
        close(in[1]);
        dup2(in[0], STDIN_FILENO);
        close(in[0]);
    } else if (in[0] != STDIN_FILENO && strcmp(files[0], "_stdin")) {
        fprintf(stderr, "%s: Can't redirect and pipe input and the same time\n", SHELL_NAME);
        exit(EXIT_FAILURE);
    } else if (in[0] == STDIN_FILENO && strcmp(files[0], "_stdin")) {
        pipe(in);

        pid_t pid = fork();
        if (pid < 0) {
            fprintf(stderr, "%s: Can't fork\n", SHELL_NAME);
            exit(EXIT_FAILURE);
        } else if (pid == 0) {
            int ch;
            FILE* file = fopen(files[0], "r");
            
            while ((ch = fgetc(file)) != EOF) { write(in[1], &ch, 1); }
            fclose(file);
            close(in[1]);
            close(in[0]);

            exit(EXIT_SUCCESS);
        } else {
            waitpid(pid, NULL, 0);

            close(in[1]);
            dup2(in[0], STDIN_FILENO);
            close(in[0]);
        }
    }

    /* Output redirection */
    if (out[1] != STDOUT_FILENO && !strcmp(files[1], "_stdout")) {
        close(out[0]);
        dup2(out[1], STDOUT_FILENO);
        close(out[1]);
    } else if (out[1] != STDOUT_FILENO && strcmp(files[1], "_stdout")) {
        fprintf(stderr, "%s: Can't redirect and pipe output and the same time\n", SHELL_NAME);
        exit(EXIT_FAILURE);
    } else if (out[1] == STDOUT_FILENO && strcmp(files[1], "_stdout")) {
        pipe(out);

        pid_t pid = fork();
        if (pid < 0) {
            fprintf(stderr, "%s: Can't fork\n", SHELL_NAME);
            exit(EXIT_FAILURE);
        } else if (pid == 0) {
            close(out[0]);
            dup2(out[1], STDOUT_FILENO);
            close(out[1]);

            if (execvp(argv[0], argv) == -1) {
                if (batchmode) fprintf(stderr, "%s: line %d: ", filename, command_no);
                fprintf(stderr, "%s: Command not found\n", argv[0]);
                exit(EXIT_FAILURE);
            }
        } else {
            int status;
            waitpid(pid, &status, 0);
            
            if (WEXITSTATUS(status) == EXIT_FAILURE)
                exit(EXIT_FAILURE);

            char ch;
            FILE* file = fopen(files[1], "w");

            fcntl(out[0], F_SETFL, O_NONBLOCK);
            while (read(out[0], &ch, 1) != -1) { fputc(ch, file); }
            close(out[0]);
            close(out[1]);
            fclose(file);

            exit(EXIT_SUCCESS);
        }
    }

    if (execvp(argv[0], argv) == -1) {
        if (batchmode)
            fprintf(stderr, "%s: line %d: ", filename, command_no);
        fprintf(stderr, "%s: Command not found\n", argv[0]);
        exit(EXIT_FAILURE);
    }
}