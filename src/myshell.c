#include "myshell.h"

void main(int argc, char *argv[]) {
    FILE *input_stream;
    int prev_command_exit = TRUE, batchmode = FALSE; // booleans
    int in[2], out[2], fd[2], lines = -1;
    char *command, line[INPUT_SIZE];
    const char delimiters[] = {';', '&', '|', '\n'};

    /* IDENTIFY SHELL MODE */
    if (argc == 1) {
        batchmode = FALSE;
        input_stream = stdin;
    } else if (argc == 2) {
        batchmode = TRUE;
        input_stream = fopen(argv[1], "r");
        
        if (input_stream == NULL) {
            fprintf(stderr, "%s: No file named %s\n", SHELL_NAME, argv[1]);
            exit(EXIT_FAILURE);
        } else {
            while(!feof(input_stream))
                if(fgetc(input_stream) == '\n')
                    lines++;
            fclose(input_stream);
            input_stream = fopen(argv[1], "r");
        }
    } else {
        fprintf(stderr, "%s: Too many input arguments\n", SHELL_NAME);
        exit(EXIT_FAILURE);
    }

    /* SHELL LOOP */
    for (int command_no; command_no < lines || lines < 0 ; command_no++) {
        if (batchmode == FALSE)
            printf("%s ", PROMPT_MSG);
        fgets(line, INPUT_SIZE, input_stream);

        /* Skip comments */
        if (line[0] == '#')
            continue;

        char* line_cp = strdup(line), *start_ptr = line_cp;

        /* COMMAND PROCESSING */
        command = strtok_r(line_cp, delimiters, &line_cp);
        while (command != NULL) {
            /* BUILT-IN COMMANDS */
            if (!strcmp(clear_string(command, ' '), "quit")) {
                exit(EXIT_SUCCESS);
            }

            if (!strncmp(clear_string(command, ' '), "cd", 2)) {
                char new_path[INPUT_SIZE];
                
                strcpy(new_path, clear_string(command, ' ')+2);

                if (new_path[0] == 0)       // cd with no args
                    strcpy(new_path, getpwuid(getuid())->pw_dir);

                if (new_path[0] == '~') {   //e.g. cd ~/Desktop
                    char tmp[INPUT_SIZE];
                    
                    strcpy(tmp, getpwuid(getuid())->pw_dir);
                    strcat(tmp, &new_path[1]);
                    strcpy(new_path, tmp);
                }

                if (chdir(new_path) != 0)
                    fprintf(stderr, "cd to %s failed\n", new_path);
                
                command = strtok_r(line_cp, delimiters, &line_cp);
                continue;
            }

            /* Initialize input/output file descriptors with std input/output */
            in[0] = STDIN_FILENO; in[1] = NULL;
            out[1] = STDOUT_FILENO; out[0] = NULL;

            // First character before and after command
            char ch[] = {line[command-start_ptr-1], line[command-start_ptr+strlen(command)]};

            /* Procede to next line due to previous command's failure */
            if (line[command-start_ptr-2] == '&' && ch[0] == '&' && prev_command_exit == EXIT_FAILURE) {
                command = strtok_r(line_cp, delimiters, &line_cp);
                continue;
            }

            /* Open necessary pipes */
            if (ch[0] != '|' && ch[1] == '|') {
                in[0] = STDIN_FILENO; in[1] = NULL;

                if (pipe(fd) < 0)
                    fprintf(stderr, "%s: Can't open pipe", SHELL_NAME);
                
                out[1] = fd[1]; out[0] = fd[0];
            } else if (ch[0] == '|' && ch[1] == '|') {
                in[0] = fd[0]; in[1] = fd[1];

                if (pipe(fd) < 0)
                    fprintf(stderr, "%s: Can't open pipe", SHELL_NAME);
                
                out[1] = fd[1]; out[0] = fd[0];
            } else if (ch[0] == '|' && ch[1] != '|') {
                in[0] = fd[0]; in[1] = fd[1];
                out[1] = STDOUT_FILENO; out[0] = NULL;
            }

            /* Child process */
            pid_t pid = fork();

            if (pid < 0) {
                printf(stderr, "%s: Fork failed\n", SHELL_NAME);
            } else if (pid == 0) {
                execute(command, in, out, argv[1], command_no, batchmode);
            }

            /* Waiting for child process to stop before continuing */
            int status;

            waitpid(pid, &status, 0);
            prev_command_exit = WEXITSTATUS(status);

            /* Close open pipes */
            if (in[0] != STDIN_FILENO)
                close(in[0]);
            if (out[1] != STDOUT_FILENO)
                close(out[1]);

            /* Update command */
            command = strtok_r(line_cp, delimiters, &line_cp);
        }
    }

    fclose(input_stream);
    exit(EXIT_SUCCESS);
}