#include "myshell.h"

char** split_line(char *line, const char *delimiters) {
    int idx = 0;
    char *line_cp = strdup(line);
    char **arguments = malloc(ARG_SIZE * sizeof(char*));

    arguments[0] = strtok_r(line_cp, delimiters, &line_cp);
    for (idx = 0; arguments[idx] != NULL; ) {
        arguments[++idx] = strtok_r(line_cp, delimiters, &line_cp);
    }

    return arguments;
}