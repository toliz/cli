#include "myshell.h"

char* clear_string(char* str, char character) {
    int count = 0;
    char* cleared_str = strdup(str);
  
    for (int i = 0; str[i]; i++) 
        if (cleared_str[i] != character) 
            cleared_str[count++] = cleared_str[i];
    
    cleared_str[count] = '\0'; 

    return cleared_str;
}